(function () {
  if (typeof TTT === "undefined") {
    window.TTT = {};
  }

  var View = TTT.View = function (game, $el) {
    this.game = game;
    this.$el = $el;
    this.setupBoard();
    this.bindEvents();
  };

  View.prototype.bindEvents = function () {
    var view = this;

    this.$el.on("click", "li", function(event) {
      var $clickedCell = $(event.currentTarget);
      var index = $clickedCell.index();
      var pos = [Math.floor(index / 3), index % 3];
      view.makeMove($clickedCell, pos);
    });

  };

  View.prototype.makeMove = function ($square, pos) {
    this.game.playMove(pos);
    $square.addClass("filled");
    $square.addClass(this.game.currentPlayer);
    $square.append(this.game.currentPlayer);
    if (this.game.isOver()) {
      if (this.game.winner()) {
        $(this.game.winner()).addClass("winner");
        alert(this.game.currentPlayer + " won the game! Click OK for a new game!");
      }
      else {
        alert("Game ended in a draw! Click OK for a new game!")
      }

      this.game = new TTT.Game();
      this.resetBoard();
    }
  };

  View.prototype.setupBoard = function () {
    var $grid = $("<ul></ul");
    $grid.addClass("tttboard");

    this.$el.append($grid);

    for (var i = 0; i < 9; i ++) {
      var $cell = $("<li></li>");
      $grid.append($cell);
    };
  };

  View.prototype.resetBoard = function () {
    this.$el.find('li').removeClass();
    this.$el.find('li').empty();
  };

})();
